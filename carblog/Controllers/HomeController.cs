﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace carblog.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        public ActionResult Megane()
        {
            return View();
        }

        public ActionResult Opel()
        {
            return View();
        }

        public ActionResult Nissan()
        {
            return View();
        }

        public ActionResult Mazda()
        {
            return View();
        }

        public ActionResult Talisman()
        {
            return View();
        }

        public ActionResult Citroen()
        {
            return View();
        }

        public ActionResult Alaskan()
        {
            return View();
        }

        public ActionResult Mission()
        {
            return View();
        }

        public ActionResult Carrera()
        {
            return View();
        }

        public ActionResult Ferrari()
        {
            return View();
        }

        public ActionResult Ford()
        {
            return View();
        }

        public ActionResult Caddy()
        {
            return View();
        }

        public ActionResult Macan()
        {
            return View();
        }

        public ActionResult Infiniti()
        {
            return View();
        }

        public ActionResult Skoda()
        {
            return View();
        }

        public ActionResult Ibiza()
        {
            return View();
        }

        public ActionResult IbizaCupra()
        {
            return View();
        }

        public ActionResult Fiat()
        {
            return View();
        }
    }
}