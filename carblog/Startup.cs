﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(carblog.Startup))]
namespace carblog
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
