﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.Entity;
namespace carblog.Areas.Admin.Controllers
{
    [Authorize]
    public class AdminHomeController : Controller
    {
        CarblogEntities cbd = new CarblogEntities();
        // GET: Admin/Home
        public ActionResult AdminIndex()
        {
            return View();
        }
        public ActionResult Kullanicilar()
        {
            var kullaniciadi = cbd.Kayit.OrderByDescending(x => x.KullaniciAdi);
            return View(kullaniciadi);
        }
        public ActionResult Add()
        {
            return View();
        }

        public ActionResult AddUser(Kayit model)
        {
            if(ModelState.IsValid)
            {
                Kayit kayit = new Kayit();
                kayit.AdSoyad=model.AdSoyad;
                kayit.EMail=model.EMail;
                kayit.KullaniciAdi=model.KullaniciAdi;
                kayit.Password=model.Password;
                cbd.Kayit.Add(model);
                cbd.SaveChanges();
            }
            return RedirectToAction("AdminIndex");
        }

        [HttpGet]
        public ActionResult Edit(string KullaniciAdi)
        {
           var model = cbd.Kayit.Find(KullaniciAdi);
            //cbd.Kayit.FirstOrDefault(x => x.KullaniciAdi == KullaniciAdi);
            return View();
        }

        [HttpPost]
        public ActionResult Edit(Kayit model)
        {
            if(ModelState.IsValid)
            {
                cbd.Kayit.Attach(model);
                cbd.Entry(model).State = System.Data.Entity.EntityState.Modified;
                cbd.SaveChanges();
            }
            return View();
        }

        public ActionResult Delete(string KullaniciAdi)
        {
            var model = cbd.Kayit.Find(KullaniciAdi);
            return View(model);
        }

        public ActionResult DeleteConfirm(string KullaniciAdi)
        {
            var model = cbd.Kayit.Find(KullaniciAdi);
            cbd.Kayit.Remove(model);
            cbd.SaveChanges();
            return RedirectToAction("AdminIndex");
        }
    }
}