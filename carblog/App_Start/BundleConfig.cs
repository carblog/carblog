﻿using System.Web;
using System.Web.Optimization;

namespace carblog
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/custom").Include(
                     "~/Scripts/bootstrap.js",
                     "~/Scripts/waypoints.min.js",
                     "~/Scripts/jquery.flexslider-min.js",
                     "~/Scripts/jquery.jplayer.min.js",
                     "~/Scripts/jquery.gray.min.js",
                     "~/Scripts/main.js",
                     "~/Scripts/custom.theme.isotope.js",
                     "~/Scripts/theme.pagebuilder.js",
                     "~/Scripts/custom.theme.js"
                     ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/styles/style.css"));
        }
    }
}
